"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _keys = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/keys"));

var _forEach = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/for-each"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/slicedToArray"));

var _entries = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/entries"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _axios = _interopRequireDefault(require("axios"));

var DEEP_INDEX_API_HOST = 'deep-index.moralis.io';
var DEEP_INDEX_SWAGGER_PATH = '/api-docs/v2/swagger.json';

var fetchSwaggerJson = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regenerator.default.mark(function _callee() {
    var http, response, result;
    return _regenerator.default.wrap(function (_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _axios.default.create({
              baseURL: "https://".concat(DEEP_INDEX_API_HOST)
            });

          case 2:
            http = _context.sent;
            _context.next = 5;
            return http.get(DEEP_INDEX_SWAGGER_PATH);

          case 5:
            response = _context.sent;
            result = response.data;
            return _context.abrupt("return", result);

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function () {
    return _ref.apply(this, arguments);
  };
}();

var getPathByTag = function (swaggerJSON) {
  var _context2;

  var pathByTag = {};
  var pathDetails = {};
  (0, _map.default)(_context2 = (0, _entries.default)(swaggerJSON.paths)).call(_context2, function (_ref2) {
    var _context3;

    var _ref3 = (0, _slicedToArray2.default)(_ref2, 2),
        pathName = _ref3[0],
        requestData = _ref3[1];

    return (0, _forEach.default)(_context3 = (0, _entries.default)(requestData)).call(_context3, function (_ref4) {
      var _ref5 = (0, _slicedToArray2.default)(_ref4, 2),
          method = _ref5[0],
          data = _ref5[1];

      var tags = data.tags;

      if (tags.length > 0) {
        if (!pathByTag[tags[0]]) {
          pathByTag[tags[0]] = [];
        }

        pathByTag[tags[0]].push(data.operationId);
        pathDetails[data.operationId] = {
          method: method,
          pathName: pathName,
          data: data
        };
      }
    });
  });
  return {
    pathByTag: pathByTag,
    pathDetails: pathDetails
  };
};

var fetchEndpoints = /*#__PURE__*/function () {
  var _ref6 = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regenerator.default.mark(function _callee2() {
    var _context4;

    var swaggerJSON, _yield$getPathByTag, pathDetails, data;

    return _regenerator.default.wrap(function (_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return fetchSwaggerJson();

          case 2:
            swaggerJSON = _context5.sent;
            _context5.next = 5;
            return getPathByTag(swaggerJSON);

          case 5:
            _yield$getPathByTag = _context5.sent;
            pathDetails = _yield$getPathByTag.pathDetails;
            data = [];
            (0, _forEach.default)(_context4 = (0, _keys.default)(pathDetails)).call(_context4, function (x) {
              var item = pathDetails[x];
              var endpoint = {
                method: item.method.toUpperCase(),
                group: item.data.tags[0],
                name: x,
                url: item.pathName.split('{').join(':').split('}').join('')
              };
              data.push(endpoint);
            });
            return _context5.abrupt("return", data);

          case 10:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee2);
  }));

  return function () {
    return _ref6.apply(this, arguments);
  };
}();

module.exports = {
  fetchSwaggerJson: fetchSwaggerJson,
  getPathByTag: getPathByTag,
  fetchEndpoints: fetchEndpoints
};