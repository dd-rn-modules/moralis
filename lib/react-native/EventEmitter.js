import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';

EventEmitter.prototype.on = EventEmitter.prototype.addListener;
module.exports = EventEmitter;
