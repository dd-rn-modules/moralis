"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _web = _interopRequireDefault(require("web3"));
/* global window */


var WARNING = 'Non ethereum enabled browser';

function getWeb3FromBrowser() {
  return _getWeb3FromBrowser.apply(this, arguments);
}

function _getWeb3FromBrowser() {
  _getWeb3FromBrowser = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regenerator.default.mark(function _callee3() {
    var _window$web;

    var MWeb3, provider, _window, ethereum, web3;

    return _regenerator.default.wrap(function (_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            MWeb3 = typeof _web.default === 'function' ? _web.default : window.Web3;
            provider = (_window$web = window.web3) === null || _window$web === void 0 ? void 0 : _window$web.currentProvider;
            _window = window, ethereum = _window.ethereum;

            if (!(provider !== null && provider !== void 0 && provider.isTrust)) {
              _context3.next = 5;
              break;
            }

            return _context3.abrupt("return", new MWeb3(provider));

          case 5:
            if (!ethereum) {
              _context3.next = 10;
              break;
            }

            web3 = new MWeb3(ethereum);
            _context3.next = 9;
            return ethereum.enable();

          case 9:
            return _context3.abrupt("return", web3);

          case 10:
            if (!provider) {
              _context3.next = 12;
              break;
            }

            return _context3.abrupt("return", new MWeb3(provider));

          case 12:
            throw new Error(WARNING);

          case 13:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _getWeb3FromBrowser.apply(this, arguments);
}

var MoralisInjectedProvider = /*#__PURE__*/function () {
  function MoralisInjectedProvider() {
    (0, _classCallCheck2.default)(this, MoralisInjectedProvider);
  }

  (0, _createClass2.default)(MoralisInjectedProvider, [{
    key: "type",
    get: function () {
      return 'injected';
    }
  }, {
    key: "activate",
    value: function () {
      var _activate = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regenerator.default.mark(function _callee() {
        return _regenerator.default.wrap(function (_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return getWeb3FromBrowser();

              case 2:
                this.web3 = _context.sent;
                this.isActivated = true;
                return _context.abrupt("return", this.web3);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function () {
        return _activate.apply(this, arguments);
      };
    }()
  }, {
    key: "deactivate",
    value: function () {
      var _deactivate = (0, _asyncToGenerator2.default)( /*#__PURE__*/_regenerator.default.mark(function _callee2() {
        return _regenerator.default.wrap(function (_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                this.isActivated = false;
                this.web3 = null;

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function () {
        return _deactivate.apply(this, arguments);
      };
    }()
  }]);
  return MoralisInjectedProvider;
}();

var _default = MoralisInjectedProvider;
exports.default = _default;